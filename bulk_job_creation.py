# @copyright   Copyright DataCorp-Traffic 2019. All rights reserved
# @file        bulk_job_creation.py
# @author      DCT - R & D
# @date        Sep2019
# @brief       Job operations, Feedback

#----------------------------------------------------------------------------------------------------------------------
# Change history
# V1.0 - Initial version
#----------------------------------------------------------------------------------------------------------------------
# Imports
import json
import os
import sys
#To autogenerate id
import uuid
# To get current datetime
import datetime
import pandas as pd
import argparse
import psycopg2
import time

# Client Code, Job Code, Job name, Job Description, Deadline, Site name, Lat, Lon, Task Type, Start Date, Start Time 1, End Time 1
# DC30 4659 Sandwell Sandwell 30/3/2018 ATC 1 -2.0028859 52.4630828 ATC 6/4/2018 00:00 00:00

class DCTCache():
    def __init__(self):
        self.old_client_id = None
        self.old_job_code = None
        self.old_job_id = None
        self.old_site_name = None
        self.old_site_id = None

class DCTJobSiteRecordProcessor():

    def __init__(self, db_connection, status_logger, is_dry_run):
        self.__db_connection = db_connection
        self.__status_logger = status_logger
        self.__db_cache = DCTCache()
        self.__is_dry_run = is_dry_run
        self.__client_manager = DCTClientManager(self.__db_connection, self.__db_cache, status_logger)
        self.__job_manager = DCTJobManager(self.__db_connection, self.__db_cache, status_logger)
        self.__site_manager = DCTSiteManager(self.__db_connection, self.__db_cache, status_logger)
        self.__task_manager = DCTTaskManager(self.__db_connection, self.__db_cache, status_logger)

    def doProcess(self, job_site_record):

        #1. ---- Validate ----
        if not self.__job_manager.isJobDetailsValid(client_id = None, job_site_record = job_site_record):
            print("Not Valid Job details Client id:{0}, Job Code:{1}".format(job_site_record['Client Code'], job_site_record['Job Code']))
            return False

        if not self.__site_manager.isSiteDetailsValid(client_id = None, job_id = None, job_site_record = job_site_record):
            print("Not Valid Site details Client id:{0}, Job Code:{1}, Site name:{2}".format(job_site_record['Client Code'], job_site_record['Job Code'], job_site_record['Site name']))
            return False

        if not self.__task_manager.isTaskDetailsValid(client_id = None, job_id = None, site_id = None, job_site_record = job_site_record):
            print("Not Valid Task details Client id:{0}, Job Code:{1}, Site name:{2}, Task type:{3}".format(job_site_record['Client Code'], job_site_record['Job Code'], job_site_record['Site name'], job_site_record['Task Type']))
            return False

        if self.__is_dry_run:
            #Skip create - Dry run
            return True

        #2. --- Client ----
        client_id = self.__client_manager.getClientID(job_site_record["Client Code"])
        # print("get Client Id for client_id:{0}, return value:{1}".format(job_site_record["Client Code"], client_id))
        if client_id is None:
            return self.__status_logger.recordProcessStatusAsError(job_site_record,"Unknown Client Code")

        #If client ID is changing, clear cache
        if self.__db_cache.old_client_id != client_id:
            self.__db_cache.old_client_id = client_id
            self.__db_cache.old_job_code = None
            self.__db_cache.old_job_id = None
            self.__db_cache.old_site_name = None

        #3. ---- Job ----
        job_id = self.__job_manager.doProcessJob(client_id, job_site_record)
        if job_id is None:
            #Note - Error is already logged
            return False

        #4. ---- Site -----
        site_id = self.__site_manager.doProcessSite(client_id, job_id, job_site_record)
        if site_id is None:
            #Note - Error is already logged
            return False

        #5. ---- Task -----
        task_id = self.__task_manager.createTask(client_id, job_id, site_id, job_site_record)

        if task_id is None:
            #Note - Error is already logged
            return False
        else:
            self.__status_logger.recordProcessStatusAsSuccess(job_site_record, "Task Created Successfully")
        return True

class DCTClientManager():
    def __init__(self, db_connection, db_cache, status_logger):
        self.__db_connection = db_connection
        self.__db_cache = db_cache
        self.__status_logger = status_logger

    #---- Client Helpers -------------------------------------------------------
    def getClientID(self, client_code):
        # if not found, return None

        cursor = self.__db_connection.cursor()
        query = "SELECT client_id FROM dclens_customer WHERE client_id = '" + str(client_code) + "'"
        # print("Customer query ", query)
        cursor.execute(query)
        query_output = cursor.fetchone()
        # if client id not found return none
        if query_output is None:
            client_id = None
        else:
            client_id = query_output[0]

        return client_id

class DCTJobManager():
    def __init__(self, db_connection, db_cache, status_logger):
        self.__db_connection = db_connection
        self.__db_cache = db_cache
        self.__status_logger = status_logger

    def doProcessJob(self, client_id, job_site_record):
        job_code = str(job_site_record["Job Code"]).strip()
        job_id = self.__getJobID(client_id, job_code)
        if job_id is None:
            if not self.isJobDetailsValid(client_id, job_site_record):
                return False
            job_id = self.__createJob(client_id, job_site_record)
            if job_id is None:
                return False    #DB Operation failed - Create Job
            if self.__db_cache.old_job_id != job_id:
                self.__db_cache.old_site_name = None
        self.__db_cache.old_job_code = job_code
        self.__db_cache.old_job_id = job_id
        return job_id

    # ---- Job Helper functions ------------------------------------------------
    def __getJobID(self, client_id, job_code):
        assert(client_id is not None)

        #Cache
        if job_code is not None and job_code == self.__db_cache.old_job_code:
            return self.__db_cache.old_job_id

        # if not found return None
        cursor = self.__db_connection.cursor()
        query = "SELECT job_id FROM dclens_job WHERE client_id_id = %s AND job_code = %s"
        query_input = (str(client_id), str(job_code))
        # print("get job query ", query, query_input)
        cursor.execute(query, query_input)
        query_output = cursor.fetchone()

        if query_output is None:
            job_id = None
        else:
            job_id = query_output[0]

        return job_id

    def isJobDetailsValid(self, client_id, job_site_record):
        # Note: Ignore client_id, can be None
        client_code = job_site_record["Client Code"]
        job_code = job_site_record["Job Code"]
        job_name = job_site_record["Job name"]
        job_desc = job_site_record["Job Description"]
        job_deadline = job_site_record["Deadline"]

        #Validate fields, If error - invoke recordProcessStatusAsError
        if client_code is None or 0 == len(client_code.strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Client Code Error")

        if job_code is None or 0 == len(str(job_code).strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Job Code Error")

        if len(str(job_code).strip()) > 10:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Job Code exceeding 10 characters. Len:" + str(len(job_code.strip())) )

        if job_name is None or 0 == len(job_name.strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Job Name Error")

        if job_desc is None or 0 == len(job_desc.strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Job Description Error")

        if job_deadline is None or 0 == len(job_deadline.strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Deadline Error")

        if not formatDate(str(job_deadline)):
        # try:
        #     datetime.datetime.strptime(job_deadline, '%d-%m-%Y')
        # except ValueError:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Job Deadline - Invalid Date format.Use: dd/mm/yyyy")

        return True # Valid

    def __createJob(self, client_id, job_site_record):
        assert(client_id is not None)

        client_code = job_site_record["Client Code"].strip()
        job_code = str(job_site_record["Job Code"]).strip()
        job_name = job_site_record["Job name"].strip()
        job_desc = job_site_record["Job Description"].strip()
        job_deadline = str(job_site_record["Deadline"]).strip()
        job_deadline = formatDate(str(job_deadline))
        # Autogenerated id with six unique characters
        job_id = uuid.uuid4().hex[:6].upper()
        job_state = False
        job_start_timestamp = str(datetime.datetime.now())

        try:
            cursor = self.__db_connection.cursor()
            query = "Insert Into dclens_job(job_id, client_id_id, job_name, job_code, job_description, job_state, job_start_timestamp, job_end_timestamp) Values(%s, %s, %s, %s, %s, %s, %s, %s)"
            # query_input = (job_id, client_id, job_name, job_code, job_desc, str(job_state), job_start_timestamp, str(datetime.datetime.strptime(job_deadline, "%d-%m-%Y")))
            query_input = (job_id, client_id, job_name, job_code, job_desc, str(job_state), job_start_timestamp, job_deadline)
            # print("Job query:", query, query_input)
            cursor.execute(query, query_input)
            self.__db_connection.commit()
        except Exception as e:
            self.__db_connection.rollback()
            # print("Job Creation exception", e)
            self.__status_logger.recordProcessStatusAsError(job_site_record, "Couldnot create Job. Job:"+ job_name)
            return None
        #if DB Error - Invoke recordProcessStatusAsError, return None.
        return job_id

class DCTSiteManager():
    def __init__(self, db_connection, db_cache, status_logger):
        self.__db_connection = db_connection
        self.__db_cache = db_cache
        self.__status_logger = status_logger

    def doProcessSite(self, client_id, job_id, job_site_record):
        site_name = job_site_record["Site name"].strip()
        site_id = self.__getSiteID(client_id, job_id, site_name)
        if site_id is None:
            if not self.isSiteDetailsValid(client_id, job_id, job_site_record):
                return None
            site_id = self.__createSite(client_id, job_id, job_site_record)
            if site_id is None:
                return None    #DB Operation failed - Create Site
        self.__db_cache.old_site_name = site_name
        self.__db_cache.old_site_id = site_id

        return site_id

    # ---- Site Helper functions ------------------------------------------------
    def __getSiteID(self, client_id, job_id, site_name):
        assert(client_id is not None)
        assert(job_id is not None)

        if site_name is not None and site_name == self.__db_cache.old_site_name:
            return self.__db_cache.old_site_id

        cursor = self.__db_connection.cursor()
        query = "SELECT site_id FROM dclens_site WHERE job_id_id = %s AND site_name = %s"
        query_input = (str(job_id), str(site_name))
        cursor.execute(query, query_input)
        query_output = cursor.fetchone()
        if query_output is None:
            site_id = None
        else:
            site_id = query_output[0]

        return site_id

    def isSiteDetailsValid(self, client_id, job_id, job_site_record):
        # Note: Ignore client_id, job_id, can be None

        site_name = job_site_record["Site name"]
        site_dead_line = job_site_record["Deadline"]
        lat = str(job_site_record["Lat"]).strip()
        lon = str(job_site_record["Lon"]).strip()

        #Validate fields, If error - invoke recordProcessStatusAsError
        if site_name is None or 0 == len(site_name.strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Site Name Error")

        if site_dead_line is None or 0 == len(site_dead_line.strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Deadline Error")

        if not formatDate(str(site_dead_line)):
        # try:
        #     datetime.datetime.strptime(site_dead_line, '%d-%m-%Y')
        # except ValueError:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Site Deadline - Invalid Date format. Use: dd/mm/yyyy")

        #need to improve this
        if ',' in lat:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Site Lat error")
        if ',' in lon:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Site Lon error")


        return True # Valid


    def __createSite(self, client_id, job_id, job_site_record):
        assert(client_id is not None)
        assert(job_id is not None)

        site_name = str(job_site_record["Site name"]).strip()
        site_lat = str(job_site_record["Lat"]).strip()
        site_lon = str(job_site_record["Lon"]).strip()
        site_description = ''
        deadline = str(job_site_record["Deadline"]).strip()
        deadline = formatDate(str(deadline))
        site_code = site_name[0:15]

        site_id = uuid.uuid4().hex[:8].upper()

        try:
            cursor = self.__db_connection.cursor()
            query = "Insert Into dclens_site(site_id, job_id_id, site_code, site_name, site_description, site_latitude, site_longitude, deadline) Values(%s, %s, %s, %s, %s, %s, %s, %s)"
            # query_input = (site_id, job_id, site_name, site_code, site_description, site_lat, site_lon, str(datetime.datetime.strptime(deadline, "%d-%m-%Y")))
            query_input = (site_id, job_id, site_name, site_code, site_description, site_lat, site_lon, deadline)
            # print("Create Site Query :", query, query_input)
            cursor.execute(query, query_input)
            self.__db_connection.commit()
        except Exception as e:
            self.__db_connection.rollback()
            # print("site creation error", e)
            #if DB Error - Invoke recordProcessStatusAsError, return None.
            self.__status_logger.recordProcessStatusAsError(job_site_record, "Couldnot create Site " + site_name)
            return None

        return site_id


class DCTTaskManager():
    def __init__(self, db_connection, db_cache, status_logger):
        self.__db_connection = db_connection
        self.__db_cache = db_cache
        self.__status_logger = status_logger

    def createTask(self, client_id, job_id, site_id, job_site_record):
        assert(client_id is not None)
        assert(job_id is not None)
        assert(site_id is not None)

        task_type = job_site_record['Task Type'].strip()
        start_date = str(job_site_record['Start Date']).strip()
        start_date = formatDate(str(start_date))
        start_time_1 = str(job_site_record['Start Time 1']).strip()
        end_time_1 = str(job_site_record['End Time 1']).strip()
        start_time_2 = job_site_record['Start Time 2']
        end_time_2 = job_site_record['End Time 2']
        start_time_3 = job_site_record['Start Time 3']
        end_time_3 = job_site_record['End Time 3']
        image_path = ''
        task_progress = 0

        task_id = uuid.uuid4().hex[:6].upper()

        try:
            cursor = self.__db_connection.cursor()
            query = "Insert Into dclens_task(task_id, site_id_id, task_type, start_date, start_time_1, end_time_1, start_time_2, end_time_2, start_time_3, end_time_3, task_progress, image_path) Values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            # query_input = (str(task_id), str(site_id), str(task_type), str(datetime.datetime.strptime(start_date, "%d-%m-%Y")), str(start_time_1), end_time_1, start_time_2, end_time_2, start_time_3, end_time_3, str(task_progress), image_path)
            query_input = (str(task_id), str(site_id), str(task_type), start_date, str(start_time_1), end_time_1, start_time_2, end_time_2, start_time_3, end_time_3, str(task_progress), image_path)
            print("Create Task Query :", query, query_input)
            cursor.execute(query, query_input )
            self.__db_connection.commit()
        except Exception as e:
            self.__db_connection.rollback()
            print("Task creation error", e)
            #if DB Error - Invoke recordProcessStatusAsError, return None.
            self.__status_logger.recordProcessStatusAsError(job_site_record, "Couldnot create Task " + task_type)
            return None

        return task_id

    def isTaskDetailsValid(self, client_id, job_id, site_id, job_site_record):

        task_type = job_site_record['Task Type']
        start_date = job_site_record['Start Date']
        start_time_1 = job_site_record['Start Time 1']
        end_time_1 = job_site_record['End Time 1']
        start_time_2 = job_site_record['Start Time 2']
        end_time_2 = job_site_record['End Time 2']
        start_time_3 = job_site_record['Start Time 3']
        end_time_3 = job_site_record['End Time 3']

        if task_type is None or 0 == len(str(task_type).strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Task type Error")

        if len(task_type.strip()) > 15:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Task type Exceeding 15 Characters")

        if start_date is None or 0 == len(str(start_date).strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Task Date Error")

        if not formatDate(str(start_date)):
        # try:
        #     datetime.datetime.strptime(start_date, '%d-%m-%Y')
        # except ValueError:
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Task date - Invalid Date format. Use: dd/mm/yyyy")
        # start_time_1
        if start_time_1 is None or 0 == len(str(start_time_1).strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Start time1 Error")
        start_time_1 = CorrectTime(start_time_1)
        job_site_record['Start Time 1'] = start_time_1
        if not formatTime(str(start_time_1)):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "Start time1 Invalid format. Use: hh:mm")

        # end_time_1
        if end_time_1 is None or 0 == len(str(end_time_1).strip()):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "End time1 Error")
        end_time_1 = CorrectTime(end_time_1)
        job_site_record['End Time 1'] = end_time_1
        if not formatTime(str(end_time_1)):
            return self.__status_logger.recordProcessStatusAsError(job_site_record, "End time1 Invalid format. Use: hh:mm")

        # start_time_2
        if start_time_2 is None or 0 == len(str(start_time_2).strip()):
            pass
        else:
            start_time_2 = CorrectTime(start_time_2)
            job_site_record['Start Time 2'] = start_time_2
            if not formatTime(str(start_time_2)):
                return self.__status_logger.recordProcessStatusAsError(job_site_record, "Start time2 Invalid format. Use: hh:mm")
        # end_time_2
        if end_time_2 is None or 0 == len(str(end_time_2).strip()):
            pass
        else:
            end_time_2 = CorrectTime(end_time_2)
            job_site_record['End Time 2'] = end_time_2
            if not formatTime(str(end_time_2)):
                return self.__status_logger.recordProcessStatusAsError(job_site_record, "End time2 Invalid format. Use: hh:mm")
        # start_time_3
        if start_time_3 is None or 0 == len(str(start_time_2).strip()):
            pass
        else:
            start_time_3 = CorrectTime(start_time_3)
            job_site_record['Start Time 3'] = start_time_3
            if not formatTime(str(start_time_3)):
                return self.__status_logger.recordProcessStatusAsError(job_site_record, "Start time3 Invalid format. Use: hh:mm")
        # end_time_3
        if end_time_3 is None or 0 == len(str(end_time_3).strip()):
            pass
        else:
            end_time_3 = CorrectTime(end_time_3)
            job_site_record['End Time 3'] = end_time_3
            if not formatTime(str(end_time_3)):
                return self.__status_logger.recordProcessStatusAsError(job_site_record, "End time3 Invalid format. Use: hh:mm")

        return True

# Logger class
import csv

class DCTStatusLogger():
    def __init__(self, output_file, header):
        self.csvfile = open(output_file, 'w', newline='')
        self.writer = csv.writer(self.csvfile)
        self.writer.writerow(header)
        pass

    def recordProcessStatusAsError(self, job_site_record, status_message):
        return self.recordProcessStatus(job_site_record, True, status_message)

    def recordProcessStatusAsSuccess(self, job_site_record, status_message):
        return self.recordProcessStatus(job_site_record, False, status_message)

    def recordProcessStatus(self, job_site_record, is_error, status_message):
        record = [ column for column in job_site_record] #column values
        record.append(is_error)
        record.append(status_message)
        self.writer.writerow(record)
        pass

    def close(self):
        self.csvfile.close()
        return True

def formatDate(date_str):
    for fmt in ["%d/%m/%Y", '%d-%m-%Y']:
        try:
            date = datetime.datetime.strptime(date_str, fmt)
            return date
        except ValueError:
            pass
    return None

def formatTime(time_str):
    for fmt in ["%H:%M", '%H:%M:%S']:
        try:
            time_obj = time.strptime(time_str, fmt)
            return time_obj
        except ValueError:
            pass
    return None

def CorrectTime(time_str):
    if time_str =="24:00":
        return "00:00"
    elif time_str == "24:00:00":
        return "00:00:00"
    else:
        return time_str


def main(is_dry_run = False):
    # prod
    db_config = {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'trafficlensdb',
        'USER': 'postgres',
        'PASSWORD': 'dctrnd123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
    # dev
    # db_config = {
    #     'ENGINE': 'django.db.backends.postgresql',
    #     'NAME': 'TrafficLensDB',
    #     'USER': 'postgres',
    #     'PASSWORD': 'rndteam',
    #     'HOST': '127.0.0.1',
    #     'PORT': '5432',
    # }
    try:
        db_connection = psycopg2.connect(user = db_config['USER'],
                                         password = db_config['PASSWORD'],
                                         host = db_config['HOST'],
                                         port = db_config['PORT'],
                                         database = db_config['NAME'])

     # check if db connection is succesfull
        cursor = db_connection.cursor()
    except (Exception, psycopg2.Error) as error :
       print ("Error while connecting to PostgreSQL", error)
       return

    # create arg parser
    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('--input', metavar='path', type=str, help='the path to input csv', required=True)
    my_parser.add_argument('--output', metavar='path', type=str, help='the path to output csv', required=True)
    my_parser.add_argument('--dryrun', help='boolean', required=False, action='store_true')

    args = my_parser.parse_args()

    job_file = args.input
    output_file = args.output
    if args.dryrun:
        is_dry_run = True
    print("inputs \ninput file: {0},\noutput file: {1},\nisdryrun:  {2}".format(job_file, output_file, is_dry_run))
    # validate if file is present
    if not os.path.isfile(job_file) :
        print("Input file doesnot exist. File:", job_file)
        return

    if not output_file.endswith('.csv'):
        print("Invalid output file extension File:", job_file)
        return

    if job_file:
        if job_file.endswith('.xls') or job_file.endswith('.xlsx'):
            job_df = pd.read_excel(job_file)

        elif job_file.endswith('.csv'):
            job_df = pd.read_csv(job_file, quotechar='"')

        else:
            print("Invalid Input file extension File:", job_file)
            return
    else:
        print("Input file doesnot exist. File:", job_file)
        return

    job_df = job_df.where(pd.notnull(job_df), None)# convert nan to None
    csv_header = list(job_df.columns)
    csv_header.append('Status') #header for output csv file
    csv_header.append('Remarks') #header for output csv file
    status_logger = DCTStatusLogger(output_file, csv_header)

    processor = DCTJobSiteRecordProcessor(db_connection, status_logger, is_dry_run)

    # try:
    for index, row in job_df.iterrows():
        # print("row", row)
        processor.doProcess(row)
        print(".")
    # except Exception as e:
    #     print('Caught Error: {0}'.format(e))

    # Close writer
    status_logger.close()

    # finally:
    #closing database connection.
    if(db_connection):
        # cursor.close()
        db_connection.close()

if  __name__ == "__main__":
    main()
